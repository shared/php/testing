<?php

declare(strict_types=1);

namespace Naker\Testing;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Hash;

/**
 * @author  Iqbal Maulana <iq.bluejack@gmail.com>
 */
class TestCase extends BaseTestCase
{
    public function createApplication()
    {
        $app = require $this->getAppPath();

        $app->make(Kernel::class)->bootstrap();

        Hash::setRounds(4);

        return $app;
    }

    protected function setUpTraits(): array
    {
        $uses = parent::setUpTraits();

        foreach ($uses as $class) {
            $methodName = sprintf('setUp%s', class_basename($class));

            if (!method_exists($this, $methodName)) {
                continue;
            }

            call_user_func([$this, $methodName]);
        }

        return $uses;
    }

    protected function getAppPath(): string
    {
        return __DIR__.'/../bootstrap/app.php';
    }
}
