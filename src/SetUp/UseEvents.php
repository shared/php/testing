<?php

declare(strict_types=1);

namespace Naker\Testing\SetUp;

use Illuminate\Support\Facades\Event;

/**
 * @author  Iqbal Maulana <iq.bluejack@gmail.com>
 */
trait UseEvents
{
    protected function setUpUseEvents(): void
    {
        Event::fake($this->fakeEvents ?? []);
    }
}
